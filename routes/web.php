<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
<<<<<<< HEAD
});

//Rutas con nombre (encadenando el método name)
Route::get('/usuarios', 'UserController@index')->name('users.index');
/*
 * Al hacer esto, ahora dentro de las vistas podremos hacer referencia al nombre de las rutas utilizando
 * el helper route(), pasando como primer argumento el nombre de la ruta y como segundo argumento los
 * parámetros de la ruta, tal como con la función action.
 */

Route::get('/usuarios/{user}', 'UserController@show')
    ->where('user', '[0-9]+') // Establece que el id solo serán valores numericos
    ->name('users.show');

// También podemos usar expresiones regulares 
// (ver más en https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular#La_barra_inversa_o_antibarra_%22\%22)

// Si no establecemos el parámetro anterior, la ruta siguiente no servirá

/*
 * Repaso:
 * Las rutas de tipo GET sirven para solicitar y obtener información.
 * las rutas de tipo POST sirven para enviar y procesar información.
 */

/*
 * Para crear un formulario, es necesario dos rutas. La primera se encarga de la vista del formulario.
 * La segunda ruta se encarga del procesamiento del formulario.
 */
Route::get('/usuarios/nuevo', 'UserController@create')->name('users.create');

//Route::post('/usuarios/crear', 'UserController@store');

// Podemos declarar dos rutas con la misma URL pero estas funcionan si difieren en el método.

Route::post('/usuarios', 'UserController@store');

Route::get('/usuarios/{user}/editar', 'UserController@edit')->name('users.edit')->where('user', '\d+');

Route::put('/usuarios/{user}', 'UserController@update');

Route::delete('/usuarios/{user}', 'UserController@destroy')->where('user', '\d+');

// Si el controlador solo contiene un método, debemos llamarlo solo con el nombre de la clase
// Route::get('/saludo/{name}/{nickname?}', 'WelcomeUserController');

Route::get('/saludo/nombre/{name}', 'WelcomeUserController@hi_name');

Route::get('/saludo/apodo/{nickname?}', 'WelcomeUserController@hi_nickname');
=======
	//return 'Home';
})->name('welcome');

Route::get('/usuarios', 'UserController@index')->name('users');

Route::get('/usuarios/nuevo', 'UserController@create')->name('users.create');

Route::post('/usuarios/crear','UserController@store')->name('users.store');


/*Route::get('/usuarios/{id}', function ($id) {
    return '<h1>Mostrando Detalle del Usuario: '.$id.'</h1>';
});*/

Route::get('/usuarios/{user}', 'UserController@show')->name('users.show');

Route::get('/usuarios/{user}/editar', 'UserController@edit')->name('users.edit');

Route::put('/usuarios/{user}', 'UserController@update')->name('users.update');

Route::delete('/usuarios/{user}', 'UserController@destroy')->name('users.destroy');

//

Route::get('/saludo/{name}/{nickname?}', function ($name,$nickname=null) {
	if($nickname){
		return '<h1>Bienvenido '.$name.', tu apodo es: '.$nickname.' </h1>';
	}else{
		return '<h1>Bienvenido '.$name.', no tienes apodo </h1>';
	}
})->name('saludo');



    
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
