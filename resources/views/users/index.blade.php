@extends('layout')

{{--Como pasar contenido en una sola línea--}}
@section('title', 'Listado de usuarios')

@section('content')
    <div class="d-flex justify-content-between align-items-end mb-2">
        <h1 class="pb-1">{{ $title }}</h1>

        <p>
            <a href="{{ route('users.create') }}" class="btn btn-primary">Nuevo usuario</a>
        </p>
    </div>

    @if ($users->isNotEmpty())
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Correo</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    {{--<td><a href="{{ url("usuarios/{$user->id}") }}">Ver detalles</a></td>--}}

                    {{--Usando el helper action--}}
                    {{--<td><a href="{{ action("UserController@show", ["user" => $user->id]) }}">Ver detalles</a></td>--}}
                    {{--Si se usa de este modo, la ruta se construye sola... Como si fuese reingenieria inversa--}}

                    {{--Uso del método route (si usamos el método name en las rutas)--}}
                    <td>
                        <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route("users.show", ["user" => $user->id]) }}" class="btn btn-link"><span class="oi oi-eye"></span></a>
                            <a href="{{ route("users.edit", $user) }}" class="btn btn-link"><span class="oi oi-pencil"></span></a>
                            <button type="submit" class="btn btn-link"><span class="oi oi-trash"></span></button>
                        </form>
                    </td>
                    {{--También podemos pasar directamente el modelo de Eloquent sin el arreglo--}}
                    {{--<td><a href="{{ route("users.edit", ["user" => $user]) }}">Editar</a></td>--}}
                    {{--E incluso puede ser sin el arreglo--}}
                    {{--Siendo un método útil porque prescindes de estar atento de las acciones o las direcciones de las rutas--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No hay usuarios registrados.</p>
    @endif
@endsection

{{--Si queremos reemplazar la sección lo realizamos de la siguiente manera--}}
@section('sidebar')
    {{--Si queremos solo agregar contenido, debemos agregar la siguiente directiva--}}
    {{--@parent--}}
    <br>
    <p class="text-center"><img src="{{ asset('img/Noffra.jpg') }}" alt="Noffra Pic" height="200"></p>
@endsection