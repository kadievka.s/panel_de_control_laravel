<<<<<<< HEAD
@extends('layout')

@section('title', "{$title}")

@section('content')
    <h1>Editar usuario</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <h6>Por favor, corrige los siguientes errores:</h6>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Nombre:</label>
            {{--En este caso old() intentará cargar los datos referentes al campo name
            que se encuentren en la sesión y si no los consigue cargará el valor de
            $user->name.--}}
            <input type="text" class="form-control" name="name" id="name" placeholder="Pepito Romero" value="{{ old('name', $user->name) }}">
        </div>
        <div class="form-group">
            <label for="email">Correo electrónico:</label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="pepito@example.com" value="{{ old('email', $user->email) }}">
            <small id="emailHelp" class="form-text text-muted">Nosotros jamás compartiremos tu correo electrónico con ninguna persona.</small>
        </div>

        <div class="form-group">
            <label for="password">Contraseña:</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Mayor a 6 caracteres">
        </div>

        <button type="submit" class="btn btn-primary">Actualizar usuario</button>

    </form>

    <p>
        <a href="{{ route("users.index") }}">Regresar al listado de usuarios</a>
    </p>
@endsection

@section('sidebar')
@endsection
=======
@extends('layouts.layout')

@yield('header')

@section('title')
    <title>Editar Usuario</title>
@endsection

@section('content')

    @section('contentTitle')
        <h1 class="mt-5">Editar los datos del usuario #{{$user->id}}</h1>
    @endsection

        @if($errors->any())
            <div class="alert alert-danger">
                <h6>Por favor, corregir los siguientes errores:</h6>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{url('/usuarios/'.$user->id)}}">

            {!!csrf_field()!!}

            {{ method_field('PUT') }}

           <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" name="name" id="name" value="{{$user->name}}" placeholder="Nombre Apellido" class="form-control"/>
          </div> 
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{$user->email}}" placeholder="email@valido.com" class="form-control"/>
            <small id="emailHelp" class="form-text text-muted">Tu email es completamente privado.</small>
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" name="password" id="password" value="" placeholder="mínimo 6 digitos" class="form-control"/>
          </div>
          <button type="submit" class="btn btn-primary">Aceptar</button>
          <a class="btn btn-primary" href="{{url()->previous()}}">Regresar</a>

        </form>

@endsection

@yield('footer')
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
