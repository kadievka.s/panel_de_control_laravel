@extends('layout')

@section('title', "{$title}")

@section('content')
    <div class="card">
        <h4 class="card-header">Crear usuario</h4>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <h6>Por favor, corrige los errores debajo:</h6>

                    {{--<h6>Por favor, corrige los siguientes errores:</h6>--}}
                    {{--<ul>--}}
                    {{--@foreach($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                </div>
            @endif

            <form method="POST" action="{{ url('usuarios') }}">
                {{--El middleware VerifyCsrfToken nos permite evitar que terceros puedan enviar
                 peticiones de tipo POST a nuestra aplicación y realizar ataques de tipo cross-site
                 request forgery. Para agregar un campo con el token dentro de nuestro formulario,
                 que le va a permitir a Laravel reconocer peticiones de formulario que sean
                 válidas, debemos llamar al método csrf_field()--}}
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Pepito Romero" value="{{ old('name') }}">
                    @if($errors->has('name'))
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">Correo electrónico:</label>
                    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="pepito@example.com" value="{{ old('email') }}">
                    <small id="emailHelp" class="form-text text-muted">Nosotros jamás compartiremos tu correo electrónico con ninguna persona.</small>
                    @if($errors->has('email'))
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <label for="name">Bio:</label>
                    <textarea name="bio" class="form-control" id="bio">{{old('bio')}}</textarea>
                    @if($errors->has('bio'))
                        <p class="text-danger">{{ $errors->first('bio') }}</p>
                    @endif
                </div>


                <div class="form-group">
                    <label for="profession_id">Profesión:</label>
                    <select name="profession_id" id="profession_id" class="form-control">
                        <option value="">Selecciona una profesión</option>
                        @foreach( $professions as $profession)
                            <option value="{{ $profession->id }}"{{ old('profession_id') == $profession->id ? ' selected' : '' }}>
                                {{ $profession->title }}
                            </option>

                        @endforeach
                    </select>
                    @if($errors->has('profession_id'))
                            <p class="text-danger">{{ $errors->first('profession_id') }}</p>
                    @endif
                </div>


                <div class="form-group">
                    <label for="name">Twitter:</label>
                    <input type="text" class="form-control" name="twitter" id="twitter" placeholder="https://twitter.com/Stydenet" value="{{ old('twitter') }}">
                    @if($errors->has('twitter'))
                        <p class="text-danger">{{ $errors->first('twitter') }}</p>
                    @endif @if($errors->has('url'))
                        <p class="text-danger">{{ $errors->first('twitter') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Mayor a 6 caracteres">
                    @if($errors->has('password'))
                        <p class="text-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>

                <h6>Habilidades</h6>
                @foreach($skills as $skill)
                <div class="form-check form-check-inline">
                    <input name="skills[{{$skill->id}}]"
                           class="form-check-input"
                           type="checkbox"
                           id="skill_{{$skill->name}}"
                           value="{{$skill->id}}"
                            {{ old("skills.{$skill->id}") ? 'checked' : ''}}
                            {{-- in_array($skill->id, array_wrap(old('skills'))) ? 'checked' : '' --}}
                            {{--in_array(old('skills')) && in_array($skill->id , old('skills')) ? 'checked' : ''--}}
                    >
                    <label class="form-check-label" for="skill_{{$skill->name}}">{{$skill->name}}</label>
                </div>
                @endforeach

                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-primary">Crear usuario</button>
                    <a href="{{ route("users.index") }}" class="btn btn-link">Regresar al listado de usuarios</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection
