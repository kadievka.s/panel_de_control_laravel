<<<<<<< HEAD
@extends('layout')

@section('title', "Usuario {$user->id}")

@section('content')
    <h2>{{ $title }} #{{ $user->id }}</h2>
    <br>
    <table class="table table-hover">
        <tr>
            <th>Nombre del usuario:</th>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Correo electrónico:</th>
            <td>{{ $user->email }}</td>
        </tr>
    </table>
    <p>
        {{--<a href="{{ url("/usuarios") }}">Regresar al listado de usuarios</a>--}}
        {{--Otro méyodo en vez de estarle pasando una url es la siguiente--}}

        {{--Método que devuelve una url anterior--}}
        {{--<a href="{{ url()->previous() }}">Regresar al listado de usuarios</a> --}}

        {{--Método para ejecutar una acción del controlador--}}
        {{--<a href="{{ action("UserController@index") }}">Regresar al listado de usuarios</a>--}}
        {{--Aunque este último helper no es muy utilizado--}}

        {{--Método route--}}
        <a href="{{ route("users.index") }}">Regresar al listado de usuarios</a>
    </p>
@endsection

@section('sidebar')
    <p class="text-center"><img src="{{ asset('img/user.png') }}" alt="User Pic" height="200"></p>
@endsection
=======
@extends('layouts.layout')

@yield('header')

@section('title')
    <title>Detalles del Usuario</title>
@endsection

@section('content')

    @section('contentTitle')
        <h1 class="mt-5">Detalles del Usuario #{{$user->id}}</h1>
    @endsection

    <form>

        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" name="name" id="name" value="{{$user->name}}"  class="form-control" disabled="disabled"/>
        </div> 
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{$user->email}}" class="form-control" disabled="disabled"/>
            <small id="emailHelp" class="form-text text-muted">Tu email es completamente privado.</small>
        </div>
        <div class="form-group">
            <label for="profession">Profesión</label>

            @if($user->profession['title']==null)
                <input type="text" name="profession" id="profession" value="Sin Profesión" class="form-control" disabled="disabled"/>
            @else
                <input type="text" name="profession" id="profession" value="{{$user->profession['title']}}" class="form-control" disabled="disabled"/>
            @endif

        </div>
          
        <a class="btn btn-primary" href="{{url('/usuarios/')}}">Regresar</a>

    </form>

@endsection

@yield('footer')
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
