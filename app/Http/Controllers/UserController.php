<?php

namespace App\Http\Controllers;

use App\{Http\Requests\CreateUserRequest, Skill, User, UserProfile};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Profession;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    // Si cometemos un error, en el directorio /storage/logs/ podemos observar los errores
    public function index()
    {
        //Usando el manejador de consultas de Laravel
        #$users = DB::table('users')->get();

        //Usando ORM Eloquent
        $users = User::all();

        /*
         * Al usar Eloquent, si imprimimos el objeto 'Users' no ocurrirá un error porque cada objeto (o cada)
         * usuario va a representar una instancia de la clase User. En cambio, con el constructor de consultas
         * de Laravel tendremos diversos objetos que no son instancias de un modelo de Eloquent.
         */
        #dd($users);

        $title = 'Listado de usuarios';

        //Otra forma de pasarlo
        #return view('users.index')->with('users', User::all())->with('title', 'Listado de usuarios');

        return view('users.index', compact('title', 'users'));
    }

    /*
     * En lugar de obtener el usuario utilizando los métodos find o findOrFail podemos obtenerlo directamente
     * como parámetro de la acción.
     * Para que esto funcione, nota que el nombre del parámetro en la declaración de la ruta debe coincidir
     * con el nombre del parámetro en la declaración del método.
     * Además el tipo del parámetro debe ser, por supuesto, un modelo de Eloquent
     * (en nuestro ejemplo es App\User).
     */
    public function show(User $user)
    {

        $title = "Usuario";

        return view('users.show', compact('title', 'user'));
    }

    public function create()
    {

        $title = "Crear usuario";

        $professions= Profession::orderBy('title' , 'ASC')->get();
        $skills=Skill::orderBy('name', 'ASC')->get();

        return view('users.create', compact('title','professions','skills'));
    }

    public function store(CreateUserRequest $request)
    {

        $request->save();

        return redirect()->route("users.index");
    }

    public function edit(User $user)
    {
        $title = "Editar usuario";

        return view('users.edit', compact('title', 'user'));
    }

    public function update(User $user)
    {
        $data = request()->validate([
            'name' => 'required',
            //'email' => 'required|email|unique:users,email,'.$user->id,
            //Otra forma de escribirlo
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($user->id)],
            'password' => 'nullable|between:6,50',
        ], [
            'name.required' => 'El campo nombre es obligatorio', // Forma de escribir el mensaje personalizado
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El campo email no es valido',
            'email.unique' => 'Este email ya está registrado',
            'password.between' => 'El campo password debe ser mayor a 6 caracteres',
        ]);

        if ($data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        return redirect()->route('users.show', ['user' => $user]);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }
}
