<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeUserController extends Controller
{
    // Si un controlador solo tendrá un solo método debemos llamarlo de la siguiente manera
//    public function __invoke($name, $nickname = null)
//    {
//        $name = ucfirst($name);
//
//        if ($nickname) {
//            return "Bienvenido {$name}, tu apodo es {$nickname}";
//        } else {
//            return "Bienvenido {$name}";
//        }
//    }

    public function hi_name($name)
    {
        $name = ucfirst($name);

        return "Bienvenido {$name}!";
    }

    public function hi_nickname($nickname = null)
    {
        if ($nickname) {
            return "Bienvenido {$nickname}!";
        } else {
            return "Bienvenido anonimo!";
        }
    }
}
