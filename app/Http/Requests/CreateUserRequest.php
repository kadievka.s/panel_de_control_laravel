<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\{Http\Requests\CreateUserRequest, User, UserProfile};
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required',
            //Reglas de validación vacias
            //'email' => '',
            'email' => 'required|email|unique:users,email', //unique:tabla,campo
            //Otra forma de pasar la regla
            //'email' => ['required', 'email'],
            'password' => 'required|between:6,50',
            'bio'=>'required',
            'twitter'=>'nullable|url',
            'profession_id'=>Rule::exists('professions','id')->whereNull('deleted_at')//'exists:professions,id',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es obligatorio', // Forma de escribir el mensaje personalizado
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El campo email no es valido',
            'email.unique' => 'Este email ya está registrado',
            'password.required' => 'El campo password es obligatorio',
            'password.between' => 'El campo password debe ser mayor a 6 caracteres',
            'bio.required'=>'Este campo es obligatorio',
            'twitter.url'=>'Tiene que ser una dirección de twitter válida',
        ];
    }

    public function save()
    {
        DB::transaction(function(){

            $data=$this->validated();
            $user = User::create([
                "name" => $data["name"],
                "email" => $data["email"],
                "password" => bcrypt($data["password"]),
                'profession'=>$data["profession_id"] ?? null,
            ]);

            /*UserProfile::create([
                "bio" => $data["bio"],
                "twitter" => $data["twitter"],
                'user_id'=>$user->id,
            ]);*/

            $user->profile()->create([
                "bio" => $data["bio"],
                "twitter" => array_get($data,"twitter"), /*Esto se hace para que si el usuario manipula el formulario y elimina
                                                                este campo de él, por defecto se carge el valor null*/
            ]);
        });
    }
}
