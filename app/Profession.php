<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    /*
     * La convención es nombrar al modelo con su primera letra en mayúsculas y escribirlo en singular
     * (por ejemplo: “Profession” en vez de “professions”). Si queremos agregar dos palabras en el
     * nombre, la convención es colocar la primera letra de cada palabra en mayúscula
     */

    /*
     * No es obligatorio especificar el nombre de la tabla si seguimos la convenciones de Eloquent.
     * Si el nombre de la tabla no es igual al del modelo, debemos especificarlo en el modelo
     * definiendo la propiedad $table
     */
    //protected $table = 'professions';

    /*
     * Si la tabla no trabaja con los campos created_at y updated_at debemos desactivar el timestamps
     * de la siguiente manera:
     */

    //public $timestamps = false;

    /*
     * La excepción MassAssignmentException es una forma en la que el ORM nos protege. Una vulnerabilidad de
     * asignación masiva ocurre cuando un usuario envía un parámetro inesperado mediante una solicitud y dicho
     * parámetro realiza un cambio en la base de datos que no esperabas. Por ejemplo, un usuario podría,
     * utilizando Chrome Developer Tools o herramientas similares, agregar un campo oculto llamado is_admin con
     * el valor de 1 y enviar la solicitud de registro de esta manera. Si no tienes cuidado con esto entonces
     * cualquier usuario podría convertirse en administrador de tu aplicación, con consecuencias nefastas
     * para tu sistema.
     * Para evitar esto, dentro del modelo agregamos la propiedad $fillable y asignamos como valor un array con
     * las columnas que queremos permitir que puedan ser cargadas de forma masiva:
     */

    protected $fillable = ['title'];

    /*
     * Si en Tinker, yo quiero subir un registro, solo tomará los campos que indique en el fillable, el resto
     * se ignorarán.
     */

    //Relacionar este modelo con la clase de User
    public function users() //En plural porque una profesión tiene muchos usuarios
    {
        return $this->hasMany(User::class);
    }
}
