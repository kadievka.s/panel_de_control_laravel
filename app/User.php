<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'profession_id'
    ];

    /*
     * La siguiente propiedad nos permite convertir atributos a diferentes tipos de datos dentro de un modelo
     */

    protected $casts = [
        'is_admin' => 'boolean',
    ];




    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /*
     * Además de los métodos que nos provee Eloquent, podemos crear los nuestros propios para que puedan ser
     * usados en Tinker
     */

    public function isAdmin()
    {
        return $this->is_admin;
    }

    //Método estático para conseguir usuario por email
    public static function findByEmail($email)
    {
        //static es el equivalente de usar User::where
        return static::where(compact('email'))->first();
    }

    //Relacionar este modelo con la clase de Profession
    /*
     * Eloquent determina el nombre de la llave foránea a partir del nombre del método (en este caso profession)
     * y agregando el sufijo _id. Es decir, profession_id
     */
    public function profession() //En singular porque un usuario pertenece a una sola profesión
    {
        /*
         * Si en tu base de datos el nombre de la llave foránea no sigue esta convención puedes pasar el nombre
         * de la columna como segundo argumento:
         */
        //return $this->belongsTo(Profession::class, 'id_profession');

        /*
         * Por otro lado, si el modelo padre no usa una columna id como su llave primaria o quieres relacionar
         * el modelo a una columna diferente, puedes pasar un tercer argumento especificando el nombre de la
         * columna que actuaría como llave del modelo padre:
         */
        //return $this->belongsTo(Profession::class, 'profession_name', 'name');
        /*
         * En este caso Eloquent buscará la relación entre la columna profession_name del modelo Users y la
         * columna name del modelo Profession.
         */
        return $this->belongsTo(Profession::class);
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }
}
