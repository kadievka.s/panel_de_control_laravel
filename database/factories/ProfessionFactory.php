<?php

use Faker\Generator as Faker;

$factory->define(App\Profession::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3, false), //Oraciones con 3 palabras aleatorias
        //Si no le pasamos parámetros, generará una oración aleatoria de n palabras
    ];
});
