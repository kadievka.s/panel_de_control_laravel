<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
<<<<<<< HEAD
     * Run the database seeds.
=======
     * Seed the application's database.
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
     *
     * @return void
     */
    public function run()
    {
<<<<<<< HEAD
        //Se puede usar de dos maneras, como: <<ProfessionSeeder::class>> o simplemente el
        //nombre de la clase <<'ProfessionSeeder'>>. El problema radica que cuando se
        //le indica mal nombre, Laravel no te indicará cuál es el error.
        //dd('ProfessionSeeder');
        $this->truncateTables([
            'professions',
            'skills',
            'users',
        ]);

        // $this->call(UsersTableSeeder::class);
        $this->call([
            ProfessionSeeder::class,
            UserSeeder::class,
            SkillSeeder::class
        ]);
    }

    public function truncateTables(array $tables)
    {
        //La siguiente sentencia sirve para eliminar la revisión de llaves foráneas en la BDD
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Sentencia para eliminar los registros de la tabla
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        //Activar revisión de llaves foráneas
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
=======
        $tables=[
            'professions',
            'users'
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(ProfessionSeeder::class);
        $this->call(UserSeeder::class);

>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
    }
}
