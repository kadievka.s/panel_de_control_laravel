<?php

<<<<<<< HEAD
use App\User;
use App\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
=======
use Illuminate\Database\Seeder;
use App\User;
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
<<<<<<< HEAD
        DB::insert('INSERT INTO users (name, email, password, profession_id) VALUES (:name, :email, :password, :profession_id)', [
            'name' => 'Kueylam Nieto',
            'email' => 'kueylam@mail.com',
            'password' => bcrypt('laravel'),
            'profession_id' => '1',
        ]);
        //Hecho manualmente con SQL
        /*$professions = DB::select('SELECT id FROM professions WHERE title = ?', ['Desarrollador back-end']);*/

        //Constructor de consultas de Laravel
        /*$professions = DB::table('professions')->select('id')->take(1)->get();*/

        //Obtener solo un resultado
        /*$profession = DB::table('professions')->select('id')->where('title', '=', 'Desarrollador back-end')->first();*/

        //Si queremos tener solamente el valor, se logra de la siguiente manera
        /*$professionId = DB::table('professions')->where('title', 'Desarrollador back-end')->value('id');*/

        //Laravel también tiene métodos dinámicos o métodos mágicos, ejemplo
        /*$professionId = DB::table('professions')->whereTitle('Desarrollador back-end')->value('id');*/
        //Laravel interpreta que debe buscar en la columna de title

        //Sino escribimos select, agarra todos los campos.
        //El where puede ser de la manera escrita, sin la igualdad o mediante un array asociativo
        /*$profession = DB::table('professions')->where('title', '=', 'Desarrollador back-end')->first();*/
        /*$profession = DB::table('professions')->select('id')->where('title', 'Desarrollador back-end')->first();*/
        /*$profession = DB::table('professions')->select('id')->where(['title' => 'Desarrollador back-end'])->first();*/

        /*dd($professions[0]->id);*/
        //La forma elegante son con los objetos y métodos
        /*dd($professions->first()->id);*/

        //Select con Eloquent
        $professionId = Profession::where('title', 'Desarrollador back-end')->value('id');
        /*dd($professionId);*/

        //También puedo crear un usuario con Model Factories
        #User::create([
        factory(User::class)->create([
            'name' => 'Eduardo Marquez',
            'email' => 'eduardo20.3263@gmail.com',
            'password' => bcrypt('laravel'),
            //'profession_id' => $profession->id,
            'profession_id' => $professionId,
            'is_admin' => true,
        ]);

        //Crear un usuario mediante factory sobreescribiendo una propiedad (o campo)
        factory(User::class)->create([
            'profession_id' => $professionId,
        ]);

        //Crear 48 usuarios de forma aleatoria
        factory(User::class, 48)->create();
=======
        //DB::table('users')->truncate();

        //CONSTRUCTOR DE CONSULTAS DE SQL
        
        /*DB::table('users')->insert([
        	'name'=>'Kadievka Salcedo',
        	'email'=>'kadievka@gmail.com',
        	'password'=>bcrypt('1111'),
        	'profession_id'=>1
        ]);
        DB::insert('INSERT INTO users (name, email, password) VALUES (?,?,?)', [
        	'Otro Usuario',
        	'otrousuario@gmail.com',
        	bcrypt('1111'),
        ]);*/

        //MODEL DE ELOQUENT

        User::create ([
        	'name'=>'Kadievka Salcedo',
        	'email'=>'kadievka@gmail.com',
        	'password'=>bcrypt('1111'),
        	'is_admin'=>true,
        	'profession_id'=>1
		]);

		User::create ([
        	'name'=>'Otro Usuario',
        	'email'=>'otrousuario@gmail.com',
        	'password'=>bcrypt('1111'),
            'profession_id'=>2
		]);

        
		/*factory(User::class)->create([
			'profession_id'=>2,
		]);*/

		//factory(User::class,47)->create();

        for($i=0; $i<=47; $i++){

            $randomNumber=rand(0,20);

            if($randomNumber==0){

                factory(User::class)->create([
                    'profession_id'=>null,
                ]);

            }else{

                factory(User::class)->create([
                    'profession_id'=>$randomNumber,
                ]);
                
            }
            
        }
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
    }
}
