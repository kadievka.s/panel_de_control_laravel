<?php

use App\Profession;
//También se pueden usar con alias, ejemplo:
//use App\Profession as Profesion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Escribir una consulta de manera manual
        /*DB::insert('INSERT INTO professions (title) VALUES ("Desarrollador back-end")');*/
        //El problema de esto es podemos estar expuestos a inyección SQL
        //Laravel trabaja con PDO. Es decir, que podemos trabajar con parámetros dinámicos
        /*DB::insert('INSERT INTO professions (title) VALUES (?)', ['Desarrollador back-end']);*/
        //Y si son muchos parámetros, puede ser de esta manera
        /*DB::insert('INSERT INTO professions (title) VALUES (:title)', ['title' => 'Desarrollador back-end']);*/

        //Uso del constructor de consultas de Laravel
        /*DB::table('professions')->insert([
            'title' => 'Desarrollador back-end',
        ]);*/

        //Uso del model Profession (este es el nivel más alto de Laravel)

/*
        Profession::create([
            'title' => 'Desarrollador back-end',
        ]);

        Profession::create([
            'title' => 'Desarrollador front-end',
        ]);

        Profession::create([
            'title' => 'Diseñador web',
        ]);

        DB::delete('DELETE FROM professions WHERE id = ?', [3]);

        //Otra manera de crear 'n' cantidad de registros con Model Factories
        factory(Profession::class)->times(17)->create();

*/

        $arrayProfessions=[
            'Desarrollador back-end',
            'Desarrollador front-end',
            'Médico',
            'Enfermera',
            'Bombero',
            'Policía',
            'Profesor',
            'Mecánico',
            'Químico',
            'Físico',
            'Maestra',
            'Contador',
            'Abogado',
            'Músico',
            'Escritor',
            'Vendedor',
            'Atleta',
            'Veterinario',
            'Biólogo',
        ];

        foreach($arrayProfessions as $profession){
            factory(Profession::class)->create([
                'title'=>$profession,
            ]);
        }

    }
}
