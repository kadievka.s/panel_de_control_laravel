<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
<<<<<<< HEAD
    // https://laravel.com/docs/5.5/migrations#columns
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id'); // INTEGER UNSIGNED - AUTOINCREMENT
            $table->string('name'); // VARCHAR
            $table->string('email')->unique(); //VARCHAR - UNIQUE
            //$table->string('profession', 50)->nullable(); // Agregamos una nueva columna - campo no requerido
            $table->string('password');
            $table->boolean('is_admin')->default(false);

            /*
             *  Pero existirá un problema, debido a que debe estar creada primero la tabla de professions
             *  antes de que se pueda asignar la llave foránea. O cambiamos el orden las migraciones o se
             *  se crea una nueva como en este ejemplo
             */

            //$table->integer('profession_id')->unsigned();
            //$table->foreign('profession_id')->references('id')->on('professions');
            //Otra manera de escribirlo
            //$table->unsignedInteger('profession_id');

=======
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('is_admin')->default(false);
>>>>>>> 2f42213f9d54a29f331dd2c898d535c861a91bc4
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
